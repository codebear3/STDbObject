Pod::Spec.new do |s|
  s.name             = "STDbKit"
  s.version          = '1.0'
  s.summary          = "like CoreData, object auto convert to sqlite3."
  s.description      = "this provide class of STDbObject which you can use conveniently to insert, select, update, delete."
  s.homepage         = "https://github.com/xiaoliyang/STDbKit"
  s.license          = 'MIT'
  s.author           = { "stlwtr" => "2008.yls@163.com" }
  s.source           = { :git => "https://github.com/xiaoliyang/STDbKit.git", :tag => '1.0' }
  s.platform     = :ios, '5.0'
  s.requires_arc = true
  s.source_files = 'STDbKit/**/*.{h,m}'
  s.public_header_files = 'STDbKit/Classes/*.h'
  s.library   = "sqlite3"
end
